/**
 * @file
 * Custom uWaterloo CKEditor buttons for event tracking
 */

//TODO remove anonomous function
(function () {
    /**
     * function that runs when user selection changes
     */
    function onSelectionChange(evt) {

        if (evt.editor.readOnly) {
            return;
        }

        var selection = evt.editor.getSelection().getSelectedElement();
        //Because the CTA element in editor is a fake object that returns an img tag,
        //we check that the original element is a ckcalltoaction element by checking for class 'ckcalltoaction'

        if (selection && selection.is('img') && selection.hasClass('ckcalltoaction')) {
            if (selection.hasClass('cta_track')) {
                return this.setState(CKEDITOR.TRISTATE_ON);
            }
            return this.setState(CKEDITOR.TRISTATE_OFF);
        } else {
            return this.setState(CKEDITOR.TRISTATE_DISABLED);
        }
    }

    //binds selection change function as command
    function trackCommand(editor, name) {
        this.name = name;
    }

    trackCommand.prototype = {
        exec: function (editor) {

            let element = editor.getSelection().getSelectedElement();
            if (!element) {
                return;
            }
            if (this.state === CKEDITOR.TRISTATE_ON) {
                element.removeClass('cta_track');
                return this.setState(CKEDITOR.TRISTATE_OFF);
            } else if (this.state === CKEDITOR.TRISTATE_OFF) {
                element.addClass('cta_track');
                return this.setState(CKEDITOR.TRISTATE_ON);
            }
            editor.focus();
            editor.forceNextSelectionCheck();
        }
    };

    CKEDITOR.plugins.add('cta_track', {
        init: function (editor) {
            // Register commands.
            let cta_track = editor.addCommand('cta_track', new trackCommand(editor, 'cta_track'));
            cta_track.startDisabled = true;

            // Register buttons.
            editor.ui.addButton('Track CTA', {
                label: 'Track CTA',
                command: 'cta_track',
                icon: this.path + "/icons/cta_track.png"
            });

            //Add right click menu items
            editor.addMenuItems({
                cta_track: {
                    label: "Edit CTA Track",
                    icon: this.path + 'icons/cta_track.png',
                    command: "cta_track",
                    group: 'event_track',
                    order: 1
                }
            });

            //Add JS file that defines the dialog box.
            CKEDITOR.dialog.add('cta_track', this.path + 'dialogs/cta_track.js');
            //Register command to open dialog box when button is clicked.
            editor.addCommand('cta_track', new CKEDITOR.dialogCommand('cta_track'));

            // Register state changing handlers.
            editor.on('selectionChange', CKEDITOR.tools.bind(onSelectionChange, cta_track));
        },
        onLoad: function () {
            CKEDITOR.addCss(
                '.cta_track {' +
                'background-image: ' + this.path + "/icons/analytics.png" + ';' +
                'background-repeat: no-repeat;' +
                '}'
            );
        }

    });

}());