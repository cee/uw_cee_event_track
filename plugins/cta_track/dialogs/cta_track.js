/**
 * @file
 */
//TODO remove anonomous function
(function () {
  /**
   * function that fetches link given a node id
   * @param {int} nodeid
   * @returns {string} returns string of link contained in node 
   */
  function fetchNodeLink(nodeid) {
    jQuery.ajax({
      url: Drupal.settings.basePath + "node/" + nodeid,
      async: false,
      timeout: 5000, //Max 5s of waiting

      success: function (data) {
        let parser = new DOMParser();
        data = parser.parseFromString(data, "text/html");
        data = data.getElementsByClassName("field-name-field-cta-link").item(0).innerHTML;
        data = parser.parseFromString(data, "text/html");
        data = data.getElementsByTagName("a")[0].getAttribute("href");
        return data;
      },

      fail: function (xhr, textStatus, errorThrown) {
        return errorThrown;
      }
    });
  }


  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var cta_track_dialog = function (editor) {
    var fakeCTA = editor.getSelection().getSelectedElement();
    var realCTA = editor.restoreRealElement(fakeCTA);

    return {
      title: 'Add Optional Event Tracking Label',
      minWidth: 625,
      minHeight: 150,
      contents: [{
        id: 'cta_track',
        label: 'cta_track',
        elements: [{
          type: 'text',
          id: 'cta_track_label',
          label: 'Enter a label to be put on event: ',
          required: true,
          setup: function (element) {
            this.setValue(realCTA.getAttribute('data-calltoaction-nid'));
          }
        }]
      }],
      onOk: function () {

        // Get form information.
        console.log(realCTA.getAttribute('data-calltoaction-nid'));
        cta_url = fetchNodeLink(realCTA.getAttribute('data-calltoaction-nid'));
        console.log(cta_url);
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = "";
        if (cta_url == "") {
          errors = "Cannot access node. \r\n";
        } else {
          errors = "";
        }
        if (!calltoaction_nid) {
          errors = "You must enter a node ID.\r\n";
        }

        if (errors == '') {
          // Variable used to store number of call to action (call to php function).
          var num_of_cta;

          // Use jQuery ajax call to check if nid supplied is a call to action.
          jQuery(function ($) {

            // Make the ajax call, set to synchronous so that we wait to get a response.
            $.ajax({
              type: 'get',
              async: false,
              url: Drupal.settings.basePath + "ajax/nid_exists/" + calltoaction_nid + "/uw_embedded_call_to_action",
              // If we have success set to the global variable, so that we can use it later to check for errors.
              success: function (data) {
                num_of_cta = data;
              }
            });
          });

          // If the number of call to action is 0 (meaning there are no FF with that nid), set the errors.
          if (num_of_cta == 0) {
            errors = 'You must enter a valid call to action node ID.\r\n';
          }
        }

        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        } else {
          // Create the facts/figures element.
          var ckcalltoactionNode = new CKEDITOR.dom.element('ckcalltoaction');
          // Save contents of dialog as attributes of the element.
          ckcalltoactionNode.setAttribute('data-calltoaction-nid', calltoaction_nid);

          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckcalltoaction = CKEDITOR.embedded_calltoaction.ckcalltoaction + ': ' + calltoaction_nid;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(ckcalltoactionNode, 'ckcalltoaction', 'ckcalltoaction', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.addClass('ckcalltoaction');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          } else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckcalltoaction = CKEDITOR.embedded_calltoaction.ckcalltoaction;
        }
      },
    }
  };

  CKEDITOR.dialog.add('cta_track', function (editor) {
    return cta_track_dialog(editor);
  });
})();